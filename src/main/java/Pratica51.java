import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException, SomaMatrizesIncompativeisException, ProdMatrizesIncompativeisException {
        try {
            Matriz orig = new Matriz(3, 0);
            double[][] m = orig.getMatriz();
            m[0][0] = 0.0;
            m[0][1] = 0.1;
            m[1][0] = 1.0;
            m[1][1] = 1.1;
            m[2][0] = 2.0;
            m[2][1] = 2.1;
            Matriz transp = orig.getTransposta();
            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz transposta: " + transp);            
        } catch (MatrizInvalidaException matinve) {
            System.out.println("Ocorreu um erro de dimensao invalida: "
                + matinve.getLocalizedMessage());
        }

        
        try {
            // Soma com excecao
            Matriz a1 = new Matriz(3, 2);
            double[][] ma1 = a1.getMatriz();
            ma1[0][0] = 0.0;
            ma1[0][1] = 1.0;
            ma1[1][0] = 0.0;
            ma1[1][1] = 2.0;
            ma1[2][0] = 0.0;
            ma1[2][1] = 3.0;
            Matriz b1 = new Matriz(2, 2);
            double[][] mb1 = b1.getMatriz();
            mb1[0][0] = 1.0;
            mb1[0][1] = 2.0;
            mb1[1][0] = 3.0;
            mb1[1][1] = 4.0;
            //mb[2][0] = 5.0;
            //mb[2][1] = 6.0;
            Matriz s1 = a1.soma(b1);
            System.out.println("Matriz A: " + a1);
            System.out.println("Matriz B: " + b1);
            System.out.println("Matriz A+B: " + s1);
            // Sem excecao
            Matriz a2 = new Matriz(3, 2);
            double[][] ma2 = a2.getMatriz();
            ma2[0][0] = 0.0;
            ma2[0][1] = 1.0;
            ma2[1][0] = 0.0;
            ma2[1][1] = 2.0;
            ma2[2][0] = 0.0;
            ma2[2][1] = 3.0;
            Matriz b2 = new Matriz(3, 2);
            double[][] mb2 = b2.getMatriz();
            mb2[0][0] = 1.0;
            mb2[0][1] = 2.0;
            mb2[1][0] = 3.0;
            mb2[1][1] = 4.0;
            mb2[2][0] = 5.0;
            mb2[2][1] = 6.0;
            Matriz s2 = a2.soma(b2);
            System.out.println("Matriz A: " + a2);
            System.out.println("Matriz B: " + b2);
            System.out.println("Matriz A+B: " + s2);            
        } catch (MatrizInvalidaException matinve) {
            System.out.println("Ocorreu um erro de matriz invalida: "
                + matinve.getLocalizedMessage());
        } catch (SomaMatrizesIncompativeisException somaince) {
            System.out.println("Ocorreu um erro de matrizes incompativeis: "
                + somaince.getLocalizedMessage());
        }

        try {
            // Soma sem excecao
            Matriz a2 = new Matriz(3, 2);
            double[][] ma2 = a2.getMatriz();
            ma2[0][0] = 0.0;
            ma2[0][1] = 1.0;
            ma2[1][0] = 0.0;
            ma2[1][1] = 2.0;
            ma2[2][0] = 0.0;
            ma2[2][1] = 3.0;
            Matriz b2 = new Matriz(3, 2);
            double[][] mb2 = b2.getMatriz();
            mb2[0][0] = 1.0;
            mb2[0][1] = 2.0;
            mb2[1][0] = 3.0;
            mb2[1][1] = 4.0;
            mb2[2][0] = 5.0;
            mb2[2][1] = 6.0;
            Matriz s2 = a2.soma(b2);
            System.out.println("Matriz A: " + a2);
            System.out.println("Matriz B: " + b2);
            System.out.println("Matriz A+B: " + s2);            
        } catch (MatrizInvalidaException matinve) {
            System.out.println("Ocorreu um erro de matriz invalida: "
                + matinve.getLocalizedMessage());
        } catch (SomaMatrizesIncompativeisException somaince) {
            System.out.println("Ocorreu um erro de matrizes incompativeis: "
                + somaince.getLocalizedMessage());
        }

        try {
            // Produto com excecao    
            Matriz a3 = new Matriz(2, 4);
            double[][] ma3 = a3.getMatriz();
            ma3[0][0] = 0.0;
            ma3[0][1] = 1.0;
            ma3[0][2] = 2.0;
            ma3[0][3] = 3.0;
            ma3[1][0] = 1.0;
            ma3[1][1] = 1.0;
            ma3[1][2] = 2.0;
            ma3[1][3] = 2.0;
            Matriz c1 = new Matriz(2, 2);
            double[][] mc1 = c1.getMatriz();
            mc1[0][0] = 0.0;
            mc1[0][1] = 1.0;
            mc1[1][0] = 0.0;
            mc1[1][1] = 2.0;
            //ma3[2][0] = 0.0;
            //ma3[2][1] = 3.0;        
            Matriz p1 = a3.prod(c1);
            System.out.println("Matriz A: " + a3);
            System.out.println("Matriz C: " + c1);
            System.out.println("Matriz AxC: " + p1);
        } catch (MatrizInvalidaException matinve) {
            System.out.println("Ocorreu um erro de matriz invalida: "
                + matinve.getLocalizedMessage());
        } catch (ProdMatrizesIncompativeisException prodince) {
            System.out.println("Ocorreu um erro de matrizes incompativeis: "
                + prodince.getLocalizedMessage());
        }

        try {
            // Produto sem excecao    
            Matriz c2 = new Matriz(2, 4);
            double[][] mc2 = c2.getMatriz();
            mc2[0][0] = 0.0;
            mc2[0][1] = 1.0;
            mc2[0][2] = 2.0;
            mc2[0][3] = 3.0;
            mc2[1][0] = 1.0;
            mc2[1][1] = 1.0;
            mc2[1][2] = 2.0;
            mc2[1][3] = 2.0;
            Matriz a4 = new Matriz(4, 2);
            double[][] ma4 = a4.getMatriz();
            ma4[0][0] = 0.0;
            ma4[0][1] = 1.0;
            ma4[1][0] = 0.0;
            ma4[1][1] = 2.0;
            ma4[2][0] = 0.0;
            ma4[2][1] = 3.0;
            ma4[3][0] = 0.0;
            ma4[3][1] = 4.0;           
            Matriz p2 = a4.prod(c2);
            System.out.println("Matriz A: " + a4);
            System.out.println("Matriz C: " + c2);
            System.out.println("Matriz AxC: " + p2);
        } catch (MatrizInvalidaException matinve) {
            System.out.println("Ocorreu um erro de matriz invalida: "
                + matinve.getLocalizedMessage());
        } catch (ProdMatrizesIncompativeisException prodince) {
            System.out.println("Ocorreu um erro de matrizes incompativeis: "
                + prodince.getLocalizedMessage());
        }
    }
}
