/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author LucianoTadeu
 * 
 * Esta exceção indica que uma matriz tem uma ou ambas as dimensões de tamanho menor ou igual a zero.
 */
public class MatrizInvalidaException extends Exception {
    private final int m;
    private final int n;
    
    public MatrizInvalidaException (int m, int n) {
        super(String.format("Matriz de %dx%d não pode ser criada", m, n));
        this.m = m;
        this.n = n;
    }
    public int getNumLinhas() { // Informa o número de linhas da matriz
        return m;
    }
    
    public int getNumColunas() { // Informa o número de colunas da matriz
        return n;
    }
}
